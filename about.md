---
layout: page
title: About
permalink: /about/
---

A small potato in the IT industry. Junior Web Developer, Video Game Hobbist, Boardgame Hobbist. 

Eager to learn everything, interested on all aspect. Master of none, but learning to be Jack of all trade.


#PHP #Yii2
#Python #Flask #Jinja2
#influxDB #telegraf 
#Java 
#Docker #git

---

> Generalization is better than specialization.

> Always take a step more for the others.


