---
layout: post
title:  "Markdown CheatSheet"
categories: [Cheatsheet]
---

* This line and following line are required to show table of content (it will not be displayed)
{:toc}

## Typography

~~~
**bold**
_italic_
**_bold and italic_**
`code`
~~strikethrough~~
[hyperlink](#)
[http://www.example.com](http://www.example.com)
new line<br>
new line  
~~~

## Headings

~~~
# H1 Heading
## H2 Heading
### H3 Heading
#### H4 Heading
##### H5 Heading
###### H6 Heading
~~~

## Line Break

~~~
---
~~~

## Lists

### Ordered

~~~
1. 
2.
3.
~~~

### Unordered

~~~
- 
- 
- 
~~~

## Blockquote

~~~
> Start by doing what's necessary; then do what's possible; and suddenly you are doing the impossible. --Francis of Assisi
~~~

## Code Highlight

[Supporting Type List](https://support.codebasehq.com/articles/tips-tricks/syntax-highlighting-in-markdown)

~~~
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
s = "Python syntax highlighting"
print s
```

```
No language indicated, so no syntax highlighting. 
But let's throw in a <b>tag</b>.
```
~~~

## Table

### Table 1: With Alignment

~~~
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
~~~

### Table 2: With Typography Elements

~~~
Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3
~~~

## Media

### YouTube Embedded Iframe

~~~
<div class="video-container"><iframe src="https://www.youtube.com/embed/n1a7o44WxNo" frameborder="0" allowfullscreen></iframe></div>
~~~

### Image

~~~
![Minion](http://octodex.github.com/images/minion.png)
~~~
