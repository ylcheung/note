

# Description

Nodejs is a Javascript Runtime allows developers to code with Javascript as server. Also, some features that used for servers are added.

# What makes it famous?

It brings JS applicable from frontend to backend, allows the community and libraries to imply their experience into backend development.

# Characteristic

- Fast (JS is used)
- Async processing
- No threading
- Feature-limited (lightweight)

# Modules

- Async Hooks
- Console
- Crypto
- Errors
- File System
- HTTP / HTTP/2 / HTTPS
- Path
- Query Strings
- String Decoder
- Timers
- SSL
- URL
- Utilities

# Framework
- expressjs (provide basic routing feature and server setting of Nodejs)
- koajs


