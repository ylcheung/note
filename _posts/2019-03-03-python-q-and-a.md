---
layout: post
title:  "Python Minor Tips"
categories: [Python]
---

* This line and following line are required to show table of content (it will not be displayed)
{:toc}

This page will be keep updated when more remarkable issues encountered.

## Passing variable list of argument as function input

[Reference](https://pythontips.com/2013/08/04/args-and-kwargs-in-python-explained/)

```python
def foo (*args):
    for arg in args:
        print (arg)

>>> foo (1, 2, 3)
1
2
3
>>> foo_var = (4, 5)
>>> foo(*foo_var)
4
5
>>> asdf = [6, 7]
>>> foo(*asdf)
6
7
```

```python
def foo (test1, test2):
    print(test1)
    print(test2)

>>> foo_var = {'test1':1, 'test2':345}
>>> foo(**foo_var)
1
345
```

## Ignore extra keyword arguments as function input

[Reference](https://stackoverflow.com/questions/35862094/how-to-ignore-extra-keyword-arguments-in-python)

```python
class Test(object):
    def __init__(self, id, name, **kwargs):
        self.id = id
        self.name = name

>>> test = Test(id=1, name='test', dummy="dummy")
>>> test.id
1
>>> test.name
'test'
>>> test.dummy
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'Test' object has no attribute 'dummy'
```



