---
layout: post
title:  "Toolkit Box"
categories: [Helper]
---

* This line and following line are required to show table of content (it will not be displayed)
{:toc}

# Tool Box

Here lists the tools that help my daily life.

## Development helper

### Websites

Name | Description 
--- | --- 
[Dead Link Checker](https://www.deadlinkchecker.com/website-dead-link-checker.asp) | Check if any page of the website contains any broken link. 
[IP to Long IP Converter](https://www.smartconversion.com/unit_conversion/IP_Address_Converter.aspx) | IPv4 and Integer 2-way convertion
[Epoch & Unix Timestamp <br/> Conversion Tools](https://www.epochconverter.com/) | Human date and Timestamp 2 way convertion
[JSON lint](https://jsonlint.com/) | JSON validator and prettier
[JSON parser](http://json.parser.online.fr/) | JSON object 2-way convertion
[Random string generator](http://www.unit-conversion.info/texttools/random-string-generator/) | 
[Letter Counter](https://www.lettercount.com/)|
[Regex 101](https://regex101.com/) | Regular expression builder and tester
[Diff checker](https://www.diffchecker.com/) | Text diff comparer
[.htaccess check](http://www.htaccesscheck.com/index.html) | .htaccess file checker

### Applications

Name | Description
--- | ---
[Sequel Pro](https://sequelpro.com/) | Intuitive MySQL/MariaDB GUI
[MySQL Workbench](https://www.mysql.com/products/workbench/) | Used to generate the UML diagram
[kdiff3](http://kdiff3.sourceforge.net/) | Git conflict resolver
[Sublime Text 3](https://www.sublimetext.com/) | Lightweight while extensible text editor
[Postman](https://www.getpostman.com/) | HTTP API GUI
[Sourcetree](https://www.sourcetreeapp.com/) | Git Repository Management Interface
[iTerm2](https://iterm2.com/) | Mac Terminal alternative

### Libraries

Name | Description
--- | ---
[Jekyll](https://jekyllrb.com/) ([intro](#)) | Static site generator written in Ruby.


## Life saver

Name | Description
--- | ---
[Junk call checker](https://hkjunkcall.com/?ft=31748832) | Hong Kong Phone Number check origin
