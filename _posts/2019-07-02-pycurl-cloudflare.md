---
layout: post
title:  "pycurl cannot access cloudflare hosting websites"
categories: [Python]
---

* This line and following line are required to show table of content (it will not be displayed)
{:toc}

## Issue

When using pycurl access cloudflare websites, response code will be 403, but using curl command will access normally.

## Procedure

Response from pycurl contains following line:

```
<p>The owner of this website has banned your access based on your browser's signature.</p>
```

After Googling, it indicates it's the User-Agent error.

After serval try-and-error about Cloudflare websites, when using User-Agent:PycURL series will trigger Cloudflare return 403.

## Solution

mock other agents' signature to bypass Cloudflare's checking:

```
__curl = pycurl.Curl()
__curl.setopt(pycurl.USERAGENT, <other user agents>")
```